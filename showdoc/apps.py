from django.apps import AppConfig


class ShowdocConfig(AppConfig):
    name = 'showdoc'
