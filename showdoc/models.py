from django.db import models

# Create your models here.


class Project(models.Model):
    name = models.CharField(max_length=100, verbose_name="项目名")
    add_time = models.DateTimeField(auto_now_add=True)
    update_time = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = '项目'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.name


class Catalog(models.Model):
    name = models.CharField(max_length=100, verbose_name="目录名")
    project = models.ForeignKey(to='Project', related_name='catalog', verbose_name='所属项目', on_delete=models.CASCADE)
    add_time = models.DateTimeField(auto_now_add=True)
    update_time = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = '目录'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.name


class Doc(models.Model):
    name = models.CharField(max_length=100, verbose_name="接口名称")
    catalog = models.ForeignKey(to='Catalog', related_name='doc', verbose_name='所属目录', on_delete=models.CASCADE)
    desc = models.TextField(verbose_name='文档内容')
    add_time = models.DateTimeField(auto_now_add=True)
    update_time = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'API文档'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.name

