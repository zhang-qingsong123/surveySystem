from django.shortcuts import render
from rest_framework.viewsets import ModelViewSet

from .models import Project, Catalog, Doc
from .serializers import ProjectSerializer, CatalogSerializer, DocSerializer

# Create your views here.


class ProjectViewSet(ModelViewSet):
    """
    retrieve:
        商品详情页
    list:
        商品列表页：分页、搜索、过滤、排序
    """
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer


class CatalogViewSet(ModelViewSet):
    """
    retrieve:
        商品详情页
    list:
        商品列表页：分页、搜索、过滤、排序
    """
    queryset = Catalog.objects.all()
    serializer_class = CatalogSerializer


class DocViewSet(ModelViewSet):
    """
    retrieve:
        商品详情页
    list:
        商品列表页：分页、搜索、过滤、排序
    """
    queryset = Doc.objects.all()
    serializer_class = DocSerializer
