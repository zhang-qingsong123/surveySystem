from django.dispatch import receiver
from django.db.models.signals import post_save

from django.utils.crypto import get_random_string
from .. import models


@receiver(post_save, sender=models.Survey)
def create_unique_code(**kwargs):
    """
    {'signal': <django.db.models.signals.ModelSignal object at 0x000002D939F530F0>, 
    'sender': <class 'web.models.Survey'>, 
    'instance': <Survey: Survey object (10)>, 
    'created': True, 
    'update_fields': None, 
    'raw': False, 
    'using': 'default'}
    """
    created = kwargs.get("created", False)
    if not created:
        return
    instance = kwargs.get("instance")
    
    codes = []
    count = instance.count
    for i in range(count):
        _code = get_random_string(8)
        if models.SurveyCode.objects.filter(unique_code=_code).exists():
            continue
        codes.append(models.SurveyCode(unique_code=_code, survey=instance))
    models.SurveyCode.objects.bulk_create(codes)