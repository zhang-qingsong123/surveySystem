from django.apps import AppConfig

from django.utils.module_loading import import_module

class WebConfig(AppConfig):
    name = 'web'
    verbose_name = "问卷操作管理"

    def ready(self):
        import_module("web.signals.web")