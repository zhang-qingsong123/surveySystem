import os
import xlwt
from urllib.parse import quote

from django.views.generic import TemplateView
from django.conf import settings
from django.http.response import StreamingHttpResponse

from .. import models


class IndexView(TemplateView):
    template_name = "web/index.html"
    extra_context= {"title": "欢迎使用问卷调查系统"}


class SurveyDetailView(TemplateView):

    template_name = "web/detail.html"

    extra_context= {"title": "请填写问卷详情", }
    
    # def get(self, request, *args, **kwargs):
    #     pk = kwargs.get("pk")
    #     context = self.get_context_data(**kwargs)
    #     context.update({"pk": pk})
    #     return self.render_to_response(context)


class DownloadView(TemplateView):

    def get(self, request, *args, **kwargs) :
        # path = os.path.join(settings.BASE_DIR, "templates", "web", "index.html")
        codes = models.SurveyCode.objects.filter(survey=kwargs.get("pk")).only("unique_code")
        print(codes)
        book = xlwt.Workbook()
        table = book.add_sheet("sheet1")
        # table.write(0, 0, "唯一码号")
        for index, code in enumerate(codes.iterator()):
            print(index, code.unique_code)
            table.write(index, 0, code.unique_code)
        book.save(os.path.join(settings.BASE_DIR, "唯一码.xls"))

        def iter_file(path, size=1024):
            with open(path, "rb") as f:
                for data in iter(lambda: f.read(size), b''):
                    yield data

        response = StreamingHttpResponse(iter_file(os.path.join(settings.BASE_DIR, "唯一码.xls")))
        # 文件的内容类型
        response['Content-Type'] = 'application/octet-stream'
        # 内容描述
        response['Content-Disposition'] = "attachment; filename*=utf-8''{}".format(quote("唯一码.xls"))
        return response
        


class SurveyReportView(TemplateView):

    template_name = "web/report.html"


