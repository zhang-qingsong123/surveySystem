# Generated by Django 2.2 on 2021-07-17 09:44

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ClassList',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=32, verbose_name='班级名称')),
            ],
        ),
        migrations.CreateModel(
            name='Survey',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('times', models.PositiveSmallIntegerField(verbose_name='第几次')),
                ('count', models.PositiveSmallIntegerField(verbose_name='生成多少个唯一码')),
                ('date', models.DateTimeField(auto_now_add=True, verbose_name='创建日期')),
                ('grade', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='web.ClassList', verbose_name='针对的班级')),
            ],
        ),
        migrations.CreateModel(
            name='SurveyChoice',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=64, verbose_name='标题')),
                ('score', models.PositiveSmallIntegerField()),
                ('date', models.DateTimeField(auto_now_add=True, verbose_name='创建日期')),
            ],
        ),
        migrations.CreateModel(
            name='SurveyCode',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('unique_code', models.CharField(max_length=10, unique=True)),
                ('is_used', models.BooleanField(default=False, verbose_name='是否使用')),
                ('date', models.DateTimeField(auto_now_add=True, verbose_name='创建日期')),
                ('survey', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='web.Survey')),
            ],
        ),
        migrations.CreateModel(
            name='SurveyQuestion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('survey_type', models.CharField(choices=[('choice', '单选'), ('suggest', '建议')], max_length=32, verbose_name='问题类型')),
                ('title', models.CharField(max_length=64, verbose_name='标题')),
                ('date', models.DateTimeField(auto_now_add=True, verbose_name='创建日期')),
            ],
        ),
        migrations.CreateModel(
            name='SurveyTemplate',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(help_text='模板名称', max_length=46, verbose_name='模板名称')),
                ('date', models.DateTimeField(auto_now_add=True, verbose_name='创建日期')),
                ('questions', models.ManyToManyField(to='web.SurveyQuestion')),
            ],
        ),
        migrations.CreateModel(
            name='SurveyRecord',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('score', models.PositiveSmallIntegerField(blank=True, null=True)),
                ('content', models.CharField(blank=True, max_length=1024, null=True)),
                ('date', models.DateTimeField(auto_now_add=True, verbose_name='创建日期')),
                ('choice', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='web.SurveyChoice')),
                ('question', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='web.SurveyQuestion')),
                ('survey', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='web.Survey')),
                ('survey_code', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='web.SurveyCode')),
            ],
        ),
        migrations.AddField(
            model_name='surveychoice',
            name='question',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='choices', to='web.SurveyQuestion'),
        ),
        migrations.AddField(
            model_name='survey',
            name='survey_templates',
            field=models.ManyToManyField(blank=True, to='web.SurveyTemplate', verbose_name='问卷模板'),
        ),
    ]
