from django.core.exceptions import SuspiciousOperation
from django.db import models
from django.db.models.query import QuerySet

# Create your models here.


class ClassList(models.Model):
    """班级"""
    name = models.CharField(max_length=32, verbose_name="班级名称")

    def __str__(self):
        return self.name


class Survey(models.Model):
    """问卷调查表"""
    grade = models.ForeignKey("classList", on_delete=models.CASCADE, verbose_name="针对的班级")
    times = models.PositiveSmallIntegerField(verbose_name="第几次")
    survey_templates = models.ManyToManyField("SurveyTemplate", blank=True, verbose_name="问卷模板")
    count = models.PositiveSmallIntegerField(verbose_name="生成多少个唯一码")
    date = models.DateTimeField(auto_now_add=True, verbose_name="创建日期")

    def __str__(self):
        return f"{self.grade.name}第{self.times}次问卷调查"


class SurveyCode(models.Model):
    """唯一码表"""
    unique_code = models.CharField(max_length=10, unique=True)
    survey = models.ForeignKey("Survey", on_delete=models.CASCADE)
    is_used = models.BooleanField(default=False, verbose_name="是否使用")
    date = models.DateTimeField(auto_now_add=True, verbose_name="创建日期")

    def __str__(self):
        return self.unique_code

class SurveyTemplate(models.Model):
    """问卷模板表"""
    name = models.CharField(max_length=46, help_text="模板名称", verbose_name="模板名称")
    questions = models.ManyToManyField("SurveyQuestion")
    date = models.DateTimeField(auto_now_add=True, verbose_name="创建日期")

    def __str__(self):
        return self.name

class SurveyQuestion(models.Model):
    """问卷问题表"""
    survey_type_choices = (
        ("choice", "单选"),
        ("suggest", "建议")
    )
    survey_type = models.CharField(max_length=32, choices=survey_type_choices, verbose_name="问题类型")
    title = models.CharField(max_length=64, verbose_name="标题")
    date = models.DateTimeField(auto_now_add=True, verbose_name="创建日期")

    def __str__(self):
        return self.title

class SurveyChoice(models.Model):
    """问卷选项表"""
    question = models.ForeignKey("SurveyQuestion", on_delete=models.CASCADE, related_name="choices")
    title = models.CharField(max_length=64, verbose_name="标题")
    score = models.PositiveSmallIntegerField()
    date = models.DateTimeField(auto_now_add=True, verbose_name="创建日期")

    def __str__(self):
        return self.question.title + "："+self.title


class SurveyRecord(models.Model):
    """问卷记录"""
    question = models.ForeignKey("SurveyQuestion", on_delete=models.CASCADE)
    survey_code = models.ForeignKey("SurveyCode", on_delete=models.CASCADE)
    survey = models.ForeignKey("Survey", on_delete=models.CASCADE)
    choice = models.ForeignKey("SurveyChoice", on_delete=models.CASCADE, null=True, blank=True)
    score = models.PositiveSmallIntegerField(null=True, blank=True)
    content = models.CharField(max_length=1024, null=True, blank=True)
    date = models.DateTimeField(auto_now_add=True, verbose_name="创建日期")
