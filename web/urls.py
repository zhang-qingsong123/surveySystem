from django.urls import path, re_path

from .views import basic

urlpatterns = [
    path('', basic.IndexView.as_view()),
    re_path(r'survey/(?P<pk>\d+)/', basic.SurveyDetailView.as_view()),
    re_path(r'survey/report/(?P<pk>\d+)/', basic.SurveyReportView.as_view()),
    re_path(r'(?P<pk>\d+)/download/', basic.DownloadView.as_view()),
]
