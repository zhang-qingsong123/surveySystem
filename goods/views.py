from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet
from rest_framework_extensions.mixins import CacheResponseMixin
from rest_framework.generics import ListAPIView
import logging
import logging.config
import logging.handlers
import logstash
from logstash_async.handler import AsynchronousLogstashHandler
from django.utils import log
from django.core.servers import basehttp
from django.utils.log import configure_logging

from .serializers import GoodsSerializer
from .models import Goods

# Create your views here.
# logging.handlers
# logstash.TCPLogstashHandler
# logstash.LogstashHandler
# logging.config.fileConfig()

import logging
logger = logging.getLogger(__name__)


class GoodsViewSet(CacheResponseMixin, ModelViewSet):
    """
    retrieve:
        商品详情页
    list:
        商品列表页：分页、搜索、过滤、排序
    """
    queryset = Goods.objects.all()
    serializer_class = GoodsSerializer

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        print(123)
        logger.warning("info warning...")
        return Response(serializer.data)
