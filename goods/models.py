from django.db import models

# Create your models here.


class Goods(models.Model):
    name = models.CharField(max_length=100, verbose_name="商品名")
    click_num = models.IntegerField(default=0, verbose_name="点击数")
    goods_desc = models.TextField(verbose_name='商品描述')
    add_time = models.DateTimeField(auto_now_add=True)
    image = models.ImageField(upload_to='front', null=True, blank=True, verbose_name='封面图')

    class Meta:
        verbose_name = '商品'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.name

