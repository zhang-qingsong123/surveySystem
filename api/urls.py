from django.urls import path, re_path
from api.apis import basic, other, goods

urlpatterns = [
    path('survey/', basic.SurveyApi.as_view()),
    re_path(r'survey/(?P<pk>\d+)', basic.SurveyDetailApi.as_view()),
    re_path(r'survey/report/(?P<pk>\d+)', basic.SurveyReportApi.as_view()),
    path("permission/", other.PremissionView.as_view()),
    path("goods/", goods.GoodsAPIView.as_view()),
]
