import json
from django.core.exceptions import AppRegistryNotReady
from rest_framework.views import APIView
from rest_framework import serializers
from rest_framework.response import Response
from rest_framework.generics import ListAPIView
from rest_framework.mixins import ListModelMixin
from rest_framework import viewsets



class GoodsSerializer(serializers.Serializer):
    """序列化器
    required=False，是否必须
    min_length=10, 最小长度
    """
    uuid = serializers.CharField()
    ser = serializers.CharField()
    dim = serializers.CharField()
    # metric_name = serializers.SerializerMethodField()
    metric_name = serializers.CharField()
    period = serializers.CharField()
    filter = serializers.CharField()
    time_range = serializers.CharField(required=False)
    time_start = serializers.CharField(required=False, min_length=10)
    time_till = serializers.CharField(required=False, min_length=10)

    def validate_time_start(self, value):
        return value[:10]
    def validate_time_tillt(self, value):
        return value[:10]

    # def validated_metric_name(self, value):
    #     if ',' in value:
    #         return value.split(',')
    #     return value
    
    # def get_metric_name(self, value):
    #     print(value)
        # metric_name = value['metric_name']
        # print(metric_name)
        # if ',' in value:
        #     print("list")
        #     return metric_name.split(',')
        # return value

    # def validate(self, attrs):
    #     metric_name = attrs['metric_name']
    #     if ',' in metric_name:
    #         attrs['metric_name'] = metric_name.split(',')
    #     return attrs
    # class Meta:
    #     fields = "__all__"

class GoodsAPIView(APIView):
    """监控指标详情"""
    def get(self, request):
        """监控指标详情"""
        # params = request.query_params
        # data = {'uuid': params.get('uuid')}
        serializer = GoodsSerializer(data=request.query_params)
        # print(serializer.errors)
        serializer.is_valid(raise_exception=True)  # True
        
        data = serializer.data
        metric_name = data.get('metric_name')
        if ',' in metric_name:
            metric_name = metric_name.split(',')
            data['metric_name'] = json.dumps(metric_name)
        # res = conn.xxx(data)
        # raise SyntaxError('上帝发誓')
        return Response(data)

    def post(self, request):
        print(request.data)
        print(type(request.data.get('time_start')))
        serializer = GoodsSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        print(serializer.data)
        res = serializer.data
        res.update({'a': 1, 'b': 3})
        return Response(res)