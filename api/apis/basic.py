from __future__ import print_function
from django.db.models.aggregates import Sum
from django.views import generic
from rest_framework.response import Response
from rest_framework.generics import ListAPIView, RetrieveAPIView, CreateAPIView
from rest_framework import filters
from rest_framework import pagination
from rest_framework import status

from web import models
from api.serializers import basic


class CustomFilter(filters.BaseFilterBackend):

    def filter_queryset(self, request, queryset, view):
        return queryset


class SurveyApi(ListAPIView):
    queryset = models.Survey.objects.all()
    serializer_class = basic.SurveySerializer
    filter_backends = [filters.SearchFilter, filters.OrderingFilter]
    search_fields = ["grade__name"]
    # filterset_fields = ['name', 'shop_price']
    ordering_fillds= ['grade__name']

    pagination_class = pagination.LimitOffsetPagination

    tableColumns = [
                # {"prop": "count", "label": "唯一码数量"},
                {"prop": "grade", "label": "班级"},
                # {"prop": "id", "label": "ID"},
                # {"prop": "survey_templates", "label": "关联问卷模板"},
                {"prop": "times", "label": "第几次"},
                {"prop": "valid_count", "label": "填写人次"},
                {"prop": "handle_link", "label": "填写链接"},
                {"prop": "date", "label": "日期"},
                {"prop": "handle", "label": "操作"},
            ]
    # 更改返回的结果集，重写list方法
    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        serializer = self.get_serializer(queryset, many=True)
        return Response({
                    "code": 0,
                    "data":{
                    "table_data": {"total": self.paginator.count, "data":serializer.data},
                    "table_column": self.tableColumns,
                    }
                }
            )

    def get_paginated_response(self, data):
        # print(data) # OrderedDict
        ordering = self.request.query_params.get("ordering", "")
        reverse = False
        if ordering:
            if ordering.startswith("-"):
                reverse=True
                ordering = ordering[1:]
            data.sort(key=lambda item: item[ordering], reverse=reverse)
        return Response({
            "code": 0, 
            "data":{
                "table_data": {
                    "total": self.paginator.count,
                    "data": data
                },
                "table_column": self.tableColumns,
            }
            
            }
        )


class SurveyDetailApi(RetrieveAPIView, CreateAPIView):
    
    queryset = models.Survey.objects.all()
    serializer_class = basic.SurveyDetailSerializer
    # authentication_classes = ()

    # def get_serializer_class(self):
    #     # if self.action == 'retrieve':
    #     print(self.request)
    #     if self.request.method == 'GET':
    #         return basic.SurveyDetailSerializer
    #     else:
            # return basic.SurveyCreateSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.save()
        # print(data)
        return Response(data, status=status.HTTP_201_CREATED)


class SurveyReportApi(RetrieveAPIView):
    
    def retrieve(self, request, *args, **kwargs):
        data = models.SurveyRecord.objects.filter(question__survey_type="choice", 
            survey=kwargs.get("pk")).values("question").annotate(Sum("score")).values("question__title", "score__sum")
        return Response(list(data))





