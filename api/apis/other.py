from django.contrib.auth import get_user_model
from rest_framework.generics import ListAPIView
from rest_framework.viewsets import GenericViewSet, ModelViewSet
from rest_framework.mixins import ListModelMixin, RetrieveModelMixin
from rest_framework import permissions
from rest_framework import authentication
from rest_framework.response import Response
from rest_framework import throttling


class CustomAuthentication(authentication.BaseAuthentication):

    def authenticate(self, request):
        auth_token = request.query_params.get("auth_token")
        if auth_token == "jwt":
            return (get_user_model().objects.first(), None)
        return (None, None)


class PremissionView(ListAPIView):
    permission_classes = (permissions.AllowAny, permissions.IsAdminUser)
    authentication_classes = (CustomAuthentication, authentication.SessionAuthentication, )

    throttle_classes = []

    def list(self, request, *args, **kwargs):

        return Response({"permission_classes": "AllowAny"})

