from django.core.exceptions import ValidationError
from rest_framework.views import exception_handler

def custom_exception_handler(exc, context):
    # Call REST framework's default exception handler first,
    # to get the standard error response.
    print(exc, context)
    response = exception_handler(exc, context)
    if isinstance(exc, ValidationError):
        print(response)
        response.data['code'] = 1
        
    # Now add the HTTP status code to the response.
    if response is not None:
        response.data['status_code'] = response.status_code

    return response