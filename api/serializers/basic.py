from os import error
import re
from django.db import transaction
from django.http import request
from django.template import loader
from rest_framework import serializers
from rest_framework.utils import field_mapping

from web import models

class SurveySerializer(serializers.ModelSerializer):

    grade = serializers.CharField(source="grade.name")
    valid_count = serializers.SerializerMethodField()
    handle_link = serializers.SerializerMethodField()
    handle = serializers.SerializerMethodField()
    date = serializers.DateTimeField(format="%Y-%m-%d %X")
    

    class Meta:
        model = models.Survey
        # fields = "__all__"
        fields = ["grade", "times", "valid_count", "handle_link", "handle", "date"]

    def get_valid_count(self, instance):
        """获取有效填写人次"""
        return models.SurveyCode.objects.filter(survey=instance, is_used=True).count()

    def get_handle_link(self, instance):
        """获取填写的链接"""
        request = self.context.get("request")
        return f"<a href='{request.scheme}://{request.get_host()}/survey/{instance.pk}'  target='blank'>填写</a>"

    def get_handle(self, instance):
        """获取操作链接"""
        return loader.render_to_string(
            "web/components/handle.html",
            context={
                "report_link": f"survey/report/{instance.id}/", 
                "download_link": f"{instance.id}/download/"
            }
        )


class SurveyChoiceSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.SurveyChoice
        fields = ['id', 'title', 'score']


# 问卷问题
class SruveyQuestionSerializer(serializers.ModelSerializer):

    # choices = SurveyChoiceSerializer(many=True, source="surveychoice_set.all")
    choices = SurveyChoiceSerializer(many=True)
    value = serializers.CharField(default="", error_messages={'blank': '不可为空',})
    error = serializers.CharField(default="", read_only=True)
    question = serializers.IntegerField(required=True, write_only=True)

    class Meta:
        model = models.SurveyQuestion
        fields = ["id", "survey_type", "title", "choices", "value", "error", "question"]

    def to_internal_value(self, data):
        data["question"] = self.Meta.model.objects.get(pk=data["id"])
        return data


class SurveyTemplateSerializer(serializers.ModelSerializer):

    questions = serializers.ListSerializer(child=SruveyQuestionSerializer())

    class Meta:
        model = models.SurveyTemplate
        # fields = "__all__"
        fields = [
            "id", "name", 
            "questions"]


class SurveyDetailSerializer(serializers.ModelSerializer):

    survey_templates = SurveyTemplateSerializer(many=True, read_only=True)
    unique_code = serializers.CharField(default="")
    
    class Meta:
        model = models.Survey
        fields = ["id", "survey_templates","unique_code"]


class SurveyCreateSerializer(serializers.ModelSerializer):

    survey_templates = SurveyTemplateSerializer(many=True)
    unique_code = serializers.CharField(
        error_messages={
            "required": "唯一码不可为空", "null": "唯一码不可为空", "blank": "唯一码不可为空"
        }
    )
    def validate_unique_code(self, value):
        survey = self.context.get("view").kwargs["pk"]
        code = models.SurveyCode.objects.filter(unique_code=value, is_used=False, survey=survey).first()
        if not code:
            raise serializers.ValidationError("无效唯一码")
        return code

    class Meta:
        model = models.Survey
        fields = ["id", "survey_templates", "unique_code"]

    @transaction.atomic
    def create(self, validated_data):
        # 模板
            # 问题
                # 选项(如果survey_type为choice)
        survey_templates = validated_data.get("survey_templates", [])
        unique_code = models.SurveyCode.objects.filter(unique_code=validated_data.get("unique_code")).first()

        for template in survey_templates:
            for question in template.get("questions", []):
                _data = {
                    "question_id": question.get("id"),
                    "survey_code": unique_code,
                    "survey_id": self.context.get("view").kwargs["pk"],
                }
                if question.get("survey_type") == "choice":
                    choice = models.SurveyChoice.objects.filter(pk=question.get("value")).first()
                    _data["choice"] = choice
                    _data["score"] = choice.score
                else:
                    _data["content"] = question.get("value")
                models.SurveyRecord.objects.create(**_data)
                unique_code.is_used = True
                unique_code.save(update_fields=("is_used",))
        return {"res": "created successfully"}  # 201
    



