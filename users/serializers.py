from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from django.contrib.auth import get_user_model
from django.conf import settings
import re

User = get_user_model()


class SmsSerializer(serializers.Serializer):

    mobile = serializers.CharField(max_length=11)


    def validate_mobile(self, mobile):
        """
        验证手机号码
        :param mobile:
        :return:
        """
        if User.objects.filter(mobile=mobile).count():
            raise serializers.ValidationError('用户已存在')

        if not re.match(settings.REGEX_MOBILE, mobile):
            raise serializers.ValidationError('手机号码非法')

        return mobile


class UserDetailSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ['name', 'username', 'gender', 'birthday', 'email', 'mobile']


class UserRegSerializer(serializers.ModelSerializer):
    code = serializers.CharField(write_only=True, required=True, max_length=4, help_text="验证码",
                                 error_messages={
                                     "required": "请输入验证码",
                                     "max_length": "验证码格式错误"
                                 })
    username = serializers.CharField(required=True, allow_blank=False, label='用户名',
                                     validators=[UniqueValidator(queryset=User.objects.all(), message='用户已存在')])
    password = serializers.CharField(style={'input_type': 'password'}, write_only=True, label='密码')

    # 在信号量里修改密码
    # def create(self, validated_data):
    #     user = super().create(validated_data=validated_data)
    #     user.set_password(validated_data["password"])
    #     user.save()
    #     return user

    def validate_code(self, code):
        if len(code) != 4:
            raise serializers.ValidationError("验证码错误")
        # 根据手机号和验证码从数据里查询验证码
        mobile = self.initial_data["username"]
        # 不能查到
        # 能查到但超时了
        # return code

    def validate(self, attrs):
        attrs["mobile"] = attrs["username"]
        del attrs["code"]
        return attrs

    class Meta:
        model = User
        fields = ("username", "code", "mobile", "password")
